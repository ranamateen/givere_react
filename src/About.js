import React, {useState,} from 'react';
import { useHistory } from "react-router-dom";
import './App.css';
import SellerForm from './components/SellerForm';

const About = () =>  {

  const history = useHistory();
    const goBack = () => {
        history.goBack();
    }

    

    const [name, setName] = useState("");
    const [email, setEmail] = useState("");
    const [address, setAddress] = useState(""); 
    const [phoneNo, setPhoneNo] = useState(""); 
    const [licenseNo, setLicenseNo] = useState(""); 
    const [selectedType, setSelectedType ] = useState("buyer");

    const sendEmail = async(e) =>{
        e.preventDefault();
        setHidden(!isHidden);
        let submit = {
            name,
            email,
            phoneNo,
            licenseNo,
            address
        }

        console.log("submiting data", submit);
      }

      const [isHidden, setHidden] = useState(true);
      const checkButton = (type) =>{
          setSelectedType(type);
          setHidden(!isHidden);

          console.log({useState});
      };


  return (
    
    <div>
      
    {
      
      isHidden?
      <>
      <button className="back" onClick={goBack}> &larr; Go Back</button>
      <div className = "about-div">
      <button  onClick = {checkButton.bind(this, 'buyer')} className = "search-button"> Buyer/Seller/Renter </button>
      <button  onClick = {checkButton.bind(this, 'charity')} className = "search-button"> Charity/Non-Profit </button>
      <button  onClick = {checkButton.bind(this, 'brocker')} className = "search-button"> Broker/agent. </button></div>
      </>
      : 
      <SellerForm 
          selectedType={selectedType} 
          sendEmail={sendEmail}
          setName={setName}
          setEmail={setEmail}
          setPhoneNo = {setPhoneNo}
          setAddress={setAddress}
          setLicenseNo={setLicenseNo}
      />
    }
    </div>

    
  );

}

export default About;

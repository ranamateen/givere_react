import React from 'react';
// import logo from './logo.svg';
import './App.css';
// import SideNav, { Toggle, Nav, NavItem, NavIcon, NavText } from '@trendmicro/react-sidenav';

// Be sure to include styles at some point, probably during your bootstraping
import '@trendmicro/react-sidenav/dist/react-sidenav.css';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import Nav from './Nav';
import About from './About'
import MissionStatement from './Mission'


function App() {
  return (
    <Router>
    <div className="App">
      <Nav />
      <Switch>
      <Route path = "/" exact component = {MissionStatement} />
      <Route path = "/about" exact component = {About} />
      </Switch>
    </div>
    </Router>
  );
}


const Home = (props) =>(
    <div>
    <h1>Home Page </h1>
    </div>
  );

export default App;




/* <SideNav
onSelect={(selected) => {
    // Add your code here

}}>
<SideNav.Toggle />
<SideNav.Nav defaultSelected="home">
    <NavItem eventKey="home">
        <NavIcon>
            <i className="fa fa-fw fa-home" style={{ fontSize: '1.75em' }} />
        </NavIcon>
        <NavText>
            Home
        </NavText>
    </NavItem>
    <NavItem eventKey="charts">
        <NavIcon>
            <i className="fa fa-fw fa-line-chart" style={{ fontSize: '1.75em' }} />
        </NavIcon>
        <NavText>
            Charts
        </NavText>
        <NavItem eventKey="charts/linechart">
            <NavText>
                Line Chart
            </NavText>
        </NavItem>
        <NavItem eventKey="charts/barchart">
            <NavText>
                Bar Chart
            </NavText>
        </NavItem>
    </NavItem>
</SideNav.Nav>
</SideNav> */
import React,{useState} from 'react';
import {useHistory} from 'react-router-dom'

import './App.css';

function Form() {
  const history = useHistory();
    const goBack = () => {
        history.goBack();
    }

    const [title, setTitle] = useState("");
    const [content, setContent] = useState("");
    const [contactEmail, setContactEmail] = useState(""); 

    const sendEmail = async(e) =>{
        e.preventDefault();
        let email = {
            title,
            content,
            contactEmail
        }

        console.log("sending email", email);
      

      
      };
  return (
    <div className="App">
      <button className="back" onClick={goBack}> &larr; Go Back</button>
      <React.Fragment>
      

      <form className="contact" onSubmit={sendEmail}>

          <label htmlFor="title">Seller's Address: </label>
          <input type="text" name="title" onChange={(e) => setTitle(e.target.value)} />
          
          <label htmlFor="content">Zip Code: </label>
          <textarea name="content"  onChange={(e) => setContent(e.target.value)}  ></textarea>
      
          <label htmlFor="email">License Number: </label>
          <input type="email" name="email" onChange={(e) => setContactEmail(e.target.value)} />

          <input type="submit" value="Submit" />
      </form>

  </React.Fragment>
    </div>
  );
}

export default Form;
import React from 'react';
import './App.css';
import {Link} from 'react-router-dom';
import { ReactComponent as Logo } from './givere-logo.svg';

function Nav() {
	const navStyle = {
    color: 'rgb(234, 101, 96)',
    
	};
	
  return (
    <nav>

		<Logo className="navLogo"/>
        <ul className = "nav-links">

        <Link style = {navStyle} to = "/about">
      		<li >Tell us about yourself...</li>
      	</Link>

        </ul>

    </nav>
  );
}

export default Nav;
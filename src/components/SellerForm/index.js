import React from 'react'
import './sellerForm.css'
            
function SellerForm({ selectedType, sendEmail , setName, setEmail, setPhoneNo, setLicenseNo, setAddress,  }) {
    return (
        <>
                
            { selectedType === 'buyer' ? 
                <>
                <label className = "heading" htmlFor="heading">Buyer/Seller/Renter </label>
                <form className="contact" onSubmit={sendEmail}>

                    <label htmlFor="name">Full Name: </label>
                    <input type="text" name="name" onChange={(e) => setName(e.target.value)} />
                    
                    <label htmlFor="email">Email: </label>
                    <input  type = "text" name="email" onChange={(e) => setAddress(e.target.value)} />
                    

                    <label htmlFor="phoneno">Phone Number: </label>
                    <input type="text" name="phoneno" onChange={(e) => setPhoneNo(e.target.value)} />

                    <label htmlFor="address">Address: </label>
                    <textarea type = "text" name="address"  onChange={(e) => setEmail(e.target.value)}  ></textarea>
                    

                    <input type="submit" value="Submit" />
                </form>
                </>
                : selectedType === 'charity' ? 

                <>
                <label className = "heading" htmlFor="heading">Charity/Non-Profit </label>
                <form className="contact" onSubmit={sendEmail}>
                    <label htmlFor="name">Full Name: </label>
                    <input type="text" name="name" onChange={(e) => setName(e.target.value)} />
                    
                    <label htmlFor="email">Email: </label>
                    <input  type = "text" name="email" onChange={(e) => setAddress(e.target.value)} />
                    

                    <label htmlFor="phoneno">Phone Number: </label>
                    <input type="text" name="phoneno" onChange={(e) => setPhoneNo(e.target.value)} />

                    <label htmlFor="address">Address: </label>
                    <textarea type = "text" name="address"  onChange={(e) => setEmail(e.target.value)}  ></textarea>
                    

                    <input type="submit" value="Submit" />

                </form>
                </>
                : selectedType === 'brocker' ? 
                <>
                <label className = "heading" htmlFor="heading"> Broker/Agent </label>
                <form className="contact" onSubmit={sendEmail}>

                    
                    <label htmlFor="licenseNo">Broker/Sales Person license number. : </label>
                    <input type="text" name="licenseNo" onChange={(e) => setLicenseNo(e.target.value)} />

                    <label htmlFor="address">Physical Office address: </label>
                    <input  name="address" onChange={(e) => setAddress(e.target.value)} />

                    <input type="submit" value="Submit" />
                </form>
                </>
                : null
        }
        </>
    )
}

export default SellerForm
